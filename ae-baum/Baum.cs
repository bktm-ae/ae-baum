﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ae_baum
{
    class Baum
    {
        public string Art { get; set; }
        public int Anzahl { get; set; }
        public bool Winter { get; set; }
        //Noch leer
        internal List<Blatt> blaetter = new List<Blatt>();

        /*get for List<Blatt>
        benutzt wenn List<Blatt> private, nicht internal
        public List<Blatt> getBlaetter()
        {
            return blaetter;
        }*/

        public void fruehling(int anzahl)
        {
            Anzahl = anzahl;

            for (int i = 0; i < Anzahl; i++)
            {
                //Neues Objekt wird hinten angehangen
                blaetter.Add(new Blatt());
                //Zugriff auf das ite Objekt (einfaerben)
                blaetter[i].Farbe = "hellgruen";
            }
            
        }

        public void winter()
        {
            if (Winter == true)
            {
                Anzahl = 0;
                blaetter = null;
            }
        }

    }
}
