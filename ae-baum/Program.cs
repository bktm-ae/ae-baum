﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ae_baum
{
    class Program
    {
        static void Main(string[] args)
        {
            Baum b1 = new Baum();
            b1.Art = "Buche";

            b1.Anzahl = 12;
            b1.fruehling(b1.Anzahl);
            b1.Winter = true;

            if (b1.Winter == true)
            {
                b1.winter();
                Console.WriteLine("Im Winter hat der Baum keine Blätter");

            }
            else
            {
                Console.WriteLine("Farbe der Blätter:");

                for (int i = 0; i < b1.Anzahl; i++)
                {
                    Console.WriteLine("Blatt " + i + ": " + b1.blaetter[i].Farbe);
                }
            }

            //test
            Console.ReadLine();
        }
    }
}
